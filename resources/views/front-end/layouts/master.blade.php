<!DOCTYPE html>
<html lang="en">
    <head>
        @include('front-end.includes.header')
    </head>

<body>
<!-- banner -->
    <div class="banner">
        <div class="container">
            @include('front-end.includes.banner')
        </div>
    </div>
<!-- //banner -->
<!-- welcome -->
    @yield('content')
<!-- //what -->
<!-- footer -->
<div class="footer">
    <div class="container">
    @include('front-end.includes.footer')
    </div>
</div>
<!-- //footer -->
    @include('front-end.includes.footerScript')
</body>
</html>