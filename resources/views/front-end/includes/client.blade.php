
        <h3 class="w3ls_head">Client Says About Me</h3>
        <p class="w3l">What said my client about me after completing Project. It's job feedback from <a href="https://www.upwork.com/freelancers/~01f6d2ecc051673170">Upwork</a> or <a href="https://www.fiverr.com/naim886">Fiverr</a> and local clients...</p>
        <!--screen-gallery-->
        <div class="sreen-gallery-cursual">
            <!-- required-js-files-->
            <link href="{{asset('front-end/css/owl.carousel.css')}}" rel="stylesheet">
            <script src="{{asset('front-end/js/owl.carousel.js')}}"></script>
            <script>
                $(document).ready(function() {
                    $("#owl-demo").owlCarousel({
                        items :1,
                        lazyLoad : true,
                        autoPlay : true,
                        navigation :true,
                        navigationText :  false,
                        pagination : true,
                    });
                });
            </script>
            <!--//required-js-files-->
            <div id="owl-demo" class="owl-carousel">
                <div class="item-owl">
                    <div class="customer-say">
                        <div class="col-md-6 customer-grid">
                            <div class="de_testi">
                                <div class="quotes"><img src="{{asset('front-end/images/team1.jpg')}}" alt=""></div>
                                <div class="de_testi_by">
                                    <p> "Super fast and great experience! Will definitely recommend to colleagues and wouldn't hesitate to use again."</p>
                                    <a href="#">Renee Wood </a>, Upwork Client
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 customer-grid">
                            <div class="de_testi">
                                <div class="quotes"><img src="{{asset('front-end/images/team2.jpg')}}" alt=""></div>
                                <div class="de_testi_by">
                                    <p> outstanding in timely manner and great quality. Really enjoy working with naim886, thank you!</p>
                                    <a href="#">Yasukokatsumata</a>, Fiverr Buyer
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-owl">
                    <div class="customer-say">
                        <div class="col-md-6 customer-grid">
                            <div class="de_testi">
                                <div class="quotes"><img src="{{asset('front-end/images/team3.jpg')}}" alt=""></div>
                                <div class="de_testi_by">
                                    <p> Very patient with all of our edits and listened very well to the job at hand. Did a great job and will certainly hire again.</p>
                                    <a href="#"> Sydneyrussell </a>, Fiverr Buyer
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 customer-grid">
                            <div class="de_testi">
                                <div class="quotes"><img src="{{asset('front-end/images/team4.jpg')}}" alt=""></div>
                                <div class="de_testi_by">
                                    <p> Fast and delivered exactly what I asked for. Thank yo so much Naim</p>
                                    <a href="#">Dievf228 </a>, Fiverr Buyer
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-owl">
                    <div class="customer-say">
                        <div class="col-md-6 customer-grid">
                            <div class="de_testi">
                                <div class="quotes"><img src="{{asset('front-end/images/team5.jpg')}}" alt=""></div>
                                <div class="de_testi_by">
                                    <p> He was great to work with. Completed the work promptly and allowed for some back and forth feedback so I could feel assured my head shot was to my liking. I would recommend him to anyone needing post editing work done. I will work with him for other projects as well.</p>
                                    <a href="#">Alli Burg </a>, Upwork Client
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 customer-grid">
                            <div class="de_testi">
                                <div class="quotes"><img src="{{asset('front-end/images/team6.jpg')}}" alt=""></div>
                                <div class="de_testi_by">
                                    <p>Naimul was very professional and responsive. Nevertheless he needed more time than we expected, it was not his fault because some of the images that we provided to him were poor quality and he gave his best to make them look professional. We will certainly use him in the future.</p>
                                    <a href="#">Nikolina Andric </a>, Upwork Client
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//screen-gallery-->
