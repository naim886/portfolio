

        <div class="welcome-top">
            <h2 class="w3ls_head">Welcome to My Portfolio</h2>

            <p>My Name is Md. Naimul Hasan. I'm expert in the back-end development of web applications using Laravel framework and Core PHP. I have completed my Graduation in "Computer science and Engineering" and completed Certificate course of "Advanced Web Application development using Laravel Framework". I develop web application using laravel framework for 1.5 year and using PHP 2.5 years  and doing graphics designing for 8 years in <a href="#">Upwork</a> , <a href="#">Fiverr</a> and in local market. My goal is always to meet clients' needs and deadline with 100% satisfaction.</p>
        </div>
        <div class="charitys">
            <div class="col-md-4 chrt_grid" style="visibility: visible; -webkit-animation-delay: 0.4s;">
                <div class="chrty">
                    <figure class="icon">
                        <span class="glyphicon-icon glyphicon-heart" aria-hidden="true"></span>
                    </figure>
                    <h3>Web Developer</h3>
                    <p>I develop web application using laravel framework , Core PHP , Node.js , jQuery , JavaScript , MySql , Python</p>
                </div>
            </div>
            <div class="col-md-4 chrt_grid" style="visibility: visible; -webkit-animation-delay: 0.4s;">
                <div class="chrty">
                    <figure class="icon">
                        <span class="glyphicon-icon glyphicon-asterisk" aria-hidden="true"></span>
                    </figure>
                    <h3>Web Designer</h3>
                    <p>I design website using HTML 5 , CSS 3 , JavaScript , Bootstrap</p>
                </div>
            </div>
            <div class="col-md-4 chrt_grid" style="visibility: visible; -webkit-animation-delay: 0.4s;">
                <div class="chrty">
                    <figure class="icon">
                        <span class="glyphicon-icon glyphicon-flag" aria-hidden="true"></span>
                    </figure>
                    <h3>Graphics Design</h3>
                    <p>I am also a Graphics designer, I design Logo , Flyer , Brochure , Poster , Leaflet , t-shirt , Business Card , UI/UX , Web (PSD) , All kind of Design , Photo Editing , Photography</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="banner-grids">
            <div class="col-md-8 banner-grid1">

                <h5>You can discuss with me about your application, website or desing and also you can hire me in <a href="https://www.upwork.com/freelancers/~01f6d2ecc051673170">Upwork</a> or <a href="https://www.fiverr.com/naim886">Fiverr</a> or personally... </h5>
                <p>I am committed to give you best service with your time frame with 100% satisfaction. Your suggestions and instructions are always welcome. We can make long term working relationship. Hope you will enjoy working with me. I am available at least 8 hours of daytime (whatever your timezone is). </p>
            </div>
            <div class="col-md-4 banner-grid">
                <img src="{{asset('front-end/images/1.jpg')}}" alt=" " class="img-responsive">
            </div>
            <div class="clearfix"></div>
        </div>

