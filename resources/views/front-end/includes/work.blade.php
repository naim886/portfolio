
        <h3 class="w3ls_head">My Work Process</h3>
        <p class="w3agile">Software life cycle models describe phases of the software cycle and the order in which those phases are executed. Each phase produces deliverables required by the next phase in the life cycle. Requirements are translated into design. Code is produced according to the design which is called development phase. After coding and development the testing verifies the deliverable of the implementation phase against requirements. The testing team follows Software Testing Life Cycle (STLC) which is similar to the development cycle followed by the development team.</p>
        <div class="what-grids">
            <div class="col-md-6 what-grid">
                <img src="{{asset('front-end/images/2.jpg')}}" class="img-responsive" alt=""/>
                <div class="what-agile-info">
                    <h4>Software Development Life Cycle (SDLC)</h4>
                    <p>The development of app, website, or software is a complex process, and a wrong step in any stage of software development will cause the inevitable outcomes both for the quality of product and the entire business. It involves hard work, dedication, and expertise in software development. Software development process is lengthy and needs step-by-step techniques following.</p>
                </div>

                <img src="{{asset('front-end/images/22.png')}}" class="img-responsive" alt=""/>
                <div class="what-agile-info">
                    <h4>Feasibility Analysis</h4>
                    <p>The project team defines the entire project in details and checks the project’s feasibility. The team divides the workflow into small tasks, so that developers, testers, designers, and project managers can evaluate their tasks. They define whether it’s feasible in terms such of cost, time, functioning, reliability etc.</p>
                </div>
                <img src="{{asset('front-end/images/222.png')}}" class="img-responsive" alt=""/>
                <div class="what-agile-info">
                    <h4>Marketing Process Management</h4>
                    <p>Every marketing team needs good project management. However, a project is by definition a one-off. Sustainable revenue growth requires innovation to optimize new marketing programs and high production efficiency to scale mature ones. Efficiency without innovation results in flat-lined revenue as existing marketing programs run out of gas. Innovation without efficiency results in low marketing ROI and un-scalable random acts of marketing. Markodojo marketing process management helps you build a revenue generating marketing machine.</p>
                </div>
            </div>
            <div class="col-md-6 what-grid1">
                <div class="what-top">
                    <div class="what-left">
                        <i class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></i>
                    </div>
                    <div class="what-right">
                        <h4>Requirement gathering and analysis</h4>
                        <p>Business requirements are gathered in this phase. This phase is the main focus of the project managers and stake holders. Meetings with managers, stake holders and users are held in order to determine the requirements like; Who is going to use the system? How will they use the system? What data should be input into the system? What data should be output by the system? These are general questions that get answered during a requirements gathering phase. After requirement gathering these requirements are analyzed for their validity and the possibility of incorporating the requirements in the system to be development is also studied.

                            Finally, a Requirement Specification document is created which serves the purpose of guideline for the next phase of the model. The testing team follows the Software Testing Life Cycle and starts the Test Planning phase after the requirements analysis is completed.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="what-top1">
                    <div class="what-left">
                        <i class="glyphicon glyphicon-flash" aria-hidden="true"></i>
                    </div>
                    <div class="what-right">
                        <h4>Design</h4>
                        <p>In this phase the system and software design is prepared from the requirement specifications which were studied in the first phase. System Design helps in specifying hardware and system requirements and also helps in defining overall system architecture. The system design specifications serve as input for the next phase of the model. In this phase the testers comes up with the Test strategy, where they mention what to test, how to test.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="what-top1">
                    <div class="what-left">
                        <i class="glyphicon glyphicon-fire" aria-hidden="true"></i>
                    </div>
                    <div class="what-right">
                        <h4>Implementation / Coding</h4>
                        <p>On receiving system design documents, the work is divided in modules/units and actual coding is started. Since, in this phase the code is produced so it is the main focus for the developer. This is the longest phase of the software development life cycle.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="what-top1">
                    <div class="what-left">
                        <i class="glyphicon glyphicon-fire" aria-hidden="true"></i>
                    </div>
                    <div class="what-right">
                        <h4>Testing</h4>
                        <p>After the code is developed it is tested against the requirements to make sure that the product is actually solving the needs addressed and gathered during the requirements phase. During this phase all types of functional testing like unit testing, integration testing, system testing, acceptance testing are done as well as non-functional testing are also done.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="what-top1">
                    <div class="what-left">
                        <i class="glyphicon glyphicon-fire" aria-hidden="true"></i>
                    </div>
                    <div class="what-right">
                        <h4>Deployment</h4>
                        <p>After successful testing the product is delivered / deployed to the customer for their use. As soon as the product is given to the customers they will first do the beta testing. If any changes are required or if any bugs are caught, then they will report it to the engineering team. Once those changes are made or the bugs are fixed then the final deployment will happen.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="what-top1">
                    <div class="what-left">
                        <i class="glyphicon glyphicon-fire" aria-hidden="true"></i>
                    </div>
                    <div class="what-right">
                        <h4>Maintenance</h4>
                        <p>Once when the customers starts using the developed system then the actual problems comes up and needs to be solved from time to time. This process where the care is taken for the developed product is known as maintenance.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>


        </div>
