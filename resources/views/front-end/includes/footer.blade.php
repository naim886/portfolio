
        <div class="footer-w3layouts">
            <div class="col-md-3 footer-agileits">
                <h3>Web Development</h3>
                <ul>
                    <li>Laravel Framework</li>
                    <li>Core PHP</li>
                    <li>MySql</li>
                    <li>Python</li>
                </ul>
            </div>
            <div class="col-md-3 footer-wthree">
                <h3>Web Design</h3>
                <ul>
                    <li>HTML 5</li>
                    <li>CSS 3</li>
                    <li>JavaScript</li>
                    <li>Bootstrap</li>
                </ul>
            </div>
            <div class="col-md-3 footer-w3-agileits">
                <h3>Graphics Design</h3>
                <ul>
                    <li>Logo Design</li>
                    <li>Flyer, Brochure, t-shirt</li>
                    <li>Business Card</li>
                    <li>Photo Editing</li>
                </ul>
            </div>
            <div class="col-md-3 footer-agileits-w3layouts">
                <h3>Menu</h3>
                <ul>
                    <li><a href="{{route('front.home')}}">Home</a></li>
                    <li><a href="{{route('front.about')}}">About</a></li>
                    <li><a href="{{route('front.portfolio')}}">Portfolio</a></li>
                    <li><a href="{{route('front.services')}}">Services</a></li>
                    <li><a href="{{route('front.contact')}}">Contact</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="footer-w3-agile">
            <div class="col-md-6 w3l-footer-top">
                <h3>Newsletter</h3>
                @if(session()->has('msg'))
                  <div class="alert alert-success" >
                      {{session('msg')}}
                  </div>
                @endif
                {{Form::open(['class'=>'newsletter', 'url'=>'/newsletter', 'method'=>'post'])}}
                {{Form::email('email', null , ['class'=>'email', 'placeholder'=>'Your email...','required'=>'""'])}}
                {{Form::submit('',['type'=>'button', 'class'=>'submit'])}}
                {{Form::close()}}

                <div class="footer-agile">
                    <div class="col-md-6 footer-w3-1">
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                        <p> 54/A Kollyanpur Main Road</p>
                        <p>Kollaynpur, Dhaka-1207, Bangladesh</p>
                    </div>
                    <div class="col-md-6 footer-w3l-1">
                        <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
                        <p> +88 01719123886</p>
                        <p> +88 01711890213</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6 w3ls-social-icons">
                <h3>Follow Me</h3>
                <a class="facebook" href="https://www.facebook.com/naimcse" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="twitter" href="https://twitter.com/Naim886" target="_blank"><i class="fa fa-twitter"></i></a>
                <a class="pinterest" href="https://www.pinterest.com/naim886/"  target="_blank"><i class="fa fa-pinterest" ></i></a>
                <a class="linkedin" href="https://www.linkedin.com/in/naim886/" target="_blank"><i class="fa fa-linkedin" ></i></a>
                <a class="instagram" href="https://www.instagram.com/naim886/" target="_blank"><i class="fa fa-instagram" ></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="copy-right-agile">
            <p>© @php echo date('Y') @endphp  All rights reserved | Design &amp; Development by <a href="http://naimbd.com/">Md. Naimul Hasan</a></p>
        </div>
