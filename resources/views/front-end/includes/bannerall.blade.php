        <div class="agile-header">
            <div class="agileits-contact-info text-right">
                <ul>
                    <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:naim886@gmail.com">naim886@gmail.com</a></li>
                    <li><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>+88 01719123886</li>
                    <li><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>Skype: naim.374</li>
                </ul>
            </div>
            <div class="w3_agileits_social_media">
                <ul>
                    <li class="agileinfo_share">Find Me</li>
                    <li><a href="https://www.facebook.com/naimcse" target="_blank" class="wthree_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/Naim886" target="_blank" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    {{--<li><a href="https://www.fiverr.com/naim886" class="wthree_dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>--}}
                    <li><a href="https://www.fiverr.com/naim886" target="_blank" class="wthree_dribbble"><i aria-hidden="true">  <img src="{{asset('front-end/images/fiverr.png'  )}}" ></i></a></li>

                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="navbar-brand" href="{{route('front.home')}}">MD. Naimul Hasan</a></h1>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav class="menu menu--iris">
                    <ul class="nav navbar-nav menu__list">
                        <li class="menu__item {{request()->route()->getName()==='front.home'?'menu__item--current' :''}}"><a href="{{route('front.home')}}"  class="menu__link">Home</a></li>
                        <li class="menu__item {{request()->route()->getName()==='front.about'?'menu__item--current' :''}}"><a href="{{route('front.about')}}" class="menu__link menu__item--current">About</a></li>
                        <li class="menu__item {{request()->route()->getName()==='front.portfolio'?'menu__item--current' :''}}"><a href="{{route('front.portfolio')}}" class="menu__link menu__item--current">Portfolio</a></li>
                        <li class="menu__item {{request()->route()->getName()==='front.services'?'menu__item--current' :''}}"><a href="{{route('front.services')}}" class="menu__link">Services</a></li>
                        <li class="menu__item {{request()->route()->getName()==='front.contact'?'menu__item--current' :''}}"><a href="{{route('front.contact')}}" class="menu__link">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </nav>