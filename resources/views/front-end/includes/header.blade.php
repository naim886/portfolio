    <title>Md. Naimul Hasan | @yield('title') </title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My Name is Md. Naimul Hasan. I'm expert in back-end development of web applications using Laravel framework and Core PHP. I have completed my Graduation in Computer science and Engineering and completed Certificate course of Advanced Web Application development using Laravel Framework. I am also a Professional Graphics Designer">
    <meta name="author" content="Md. Naimul Hasan">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Md. Naimul Hasan, Naim, Web Designer, Web Developer, Graphics Designer, Laravel Developer, Bangladeshi Developer, 01719123886, +8801719123886, upwork, fiverr, freelancer, PHP developer, laravel framework Developer, Freelancing, Outsourcing, Freelancer, Bangladeshi Freelancer,  মোঃ নাইমুল হাসান , নাইম" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    @stack('css')
    <link href="{{asset('front-end/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front-end/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('front-end/css/flexslider.css')}}" type="text/css" media="screen" property="" />
    <!-- js -->
    <script type="text/javascript" src="{{asset('front-end/js/jquery-2.1.4.min.js')}}"></script>
    <!-- //js -->
    <!-- font-awesome icons -->
    <link href="{{asset('front-end/css/font-awesome.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- //font-awesome icons -->
    <link href="//fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="icon" href="{{asset('front-end/images/favicon.ico')}}" type="image/x-icon">

    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="{{asset('front-end/js/move-top.js')}}"></script>
    <script type="text/javascript" src="{{asset('front-end/js/easing.js')}}"></script>
    <script type="text/javascript">

        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
