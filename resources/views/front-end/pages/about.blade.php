@extends('front-end.layouts.masterall')
@section('title','About Me')
@section('content')
  <div class="container">
     <div class="row">
         <div class="col-md-12">
           <h1>ABOUT ME</h1>
           <h2>INTRODUCING MD. NAIMUL HASAN</h2>
           <p style="color: black">
               Welcome to my Profile. My Name is Md. Naimul Hasan. I'm expert in the back-end development of web applications using Laravel and Core PHP. I have completed my Graduation in "Computer science and Engineering" and completed Certificate course of "Advanced Web Application development using Laravel Framework". I am working with laravel for 1.5 year and with PHP 2.5 years. My goal is always to meet your needs and deadline with 100% satisfaction and working more than 8 years in Graphics Design section.
           </p>
           <h3>PROFESSIONAL SKILLS</h3>
             <div class="col-md-3">
                <h4>Web Development</h4>
                 <ul>
                     <li>Dynamic Website building</li>
                     <li>Web Based Applications</li>
                     <li>Management System (School/College/University)</li>
                     <li>Company Website</li>
                     <li>Laravel Framework</li>
                     <li>Core PHP</li>
                     <li>MySql</li>
                     <li>Python</li>
                     <li>Node.js</li>
                     <li>And Many More…</li>
                 </ul>
             </div>
             <div class="col-md-3">
                 <h4>Web Design</h4>
                 <ul>
                     <li>Web Design</li>
                     <li>WordPress Customization</li>
                     <li>HTML 5</li>
                     <li>CSS 3</li>
                     <li>JavaScript</li>
                     <li>PSD to HTML</li>
                     <li>And Many More…</li>
                 </ul>
             </div>
             <div class="col-md-3">
                 <h4>Graphics Design</h4>
                 <ul>
                     <li>Logo Design</li>
                     <li>Corporate Identity Design</li>
                     <li>Flyer Design</li>
                     <li>Leaflet Design</li>
                     <li>Brochure Design</li>
                     <li>Banner design</li>
                     <li>Billboard design</li>
                     <li>And Many More…</li>
                 </ul>
             </div>
             <div class="col-md-3">
                 <h4>Photo Editing</h4>
                 <ul>
                     <li>Photo Retouching</li>
                     <li>Photo Manipulation</li>
                     <li>Fashion Portrait,</li>
                     <li>weddings skin retouch</li>
                     <li>Colour Correction</li>
                     <li>Light Correction</li>
                     <li>Makeup beauty concept</li>
                     <li>Background Remove</li>
                     <li>Watermark Remove</li>
                     <li>JPEG To Vector Converting</li>
                     <li>GIF Animation</li>
                     <li>And Many More…</li>
                 </ul>
             </div>


         </div>
     </div>
      <hr>
      <div class="row">
          <div class="col-md-12">
              <h3>ACADEMIC QUALIFICATIONS</h3>
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th scope="col">SL</th>
                      <th scope="col">Exam Title</th>
                      <th scope="col">Institute</th>
                      <th scope="col">Subject</th>
                      <th scope="col">Passing Year</th>
                      <th scope="col">Duration</th>
                      <th scope="col">Result</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                      <th scope="col">1</th>
                      <td>B.sc (CSE)</td>
                      <td>Royal University of Dhaka</td>
                      <td>Computer Science & Engineering (CSE)</td>
                      <td>2019</td>
                      <td>4 Years</td>
                      <td>3.90 out of 4.00 (till now)</td>
                  </tr>
                  <tr>
                      <th scope="col">2</th>
                      <td>Diploma</td>
                      <td>Bangladesh Computer Council (BCC)</td>
                      <td>Information and Communications Technology</td>
                      <td>2015</td>
                      <td>8 Months</td>
                      <td>3.25 out of 4.00</td>
                  </tr>
                  <tr>
                      <th scope="col">3</th>
                      <td>BA (Honours)</td>
                      <td>Govt. Titumir College (Under NU)</td>
                      <td>English Lanugae And Literature</td>
                      <td>2014</td>
                      <td>4 Years</td>
                      <td>2.78 out of 4.00</td>
                  </tr>
                  <tr>
                      <th scope="col">4</th>
                      <td>HSC</td>
                      <td>Shahid Smrity Degree College</td>
                      <td>-</td>
                      <td>2009</td>
                      <td>2 Years</td>
                      <td>5.00 out of 5.00</td>
                  </tr>
                  <tr>
                      <th scope="col">5</th>
                      <td>SSC</td>
                      <td>Sarsina Alia Madrasha</td>
                      <td>-</td>
                      <td>2007</td>
                      <td>2 Years</td>
                      <td>4.83 out of 5.00</td>
                  </tr>
                  </tbody>

              </table>

              <h3>PROFESSIONAL CERTIFICATION </h3>
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th scope="col">SL</th>
                      <th scope="col">Course Title</th>
                      <th scope="col">Institute</th>
                      <th scope="col">Passing Year</th>
                      <th scope="col">Duration</th>
                  </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <th scope="col">1</th>
                          <td>Advanced Web Application development using Laravel Framework</td>
                          <td>Bangladesh Institute of Technology and Management | BITM</td>
                          <td>2018</td>
                          <td>90 hours (4 months)</td>
                      </tr>
                      <tr>
                          <th scope="col">1</th>
                          <td>Graphics Designing</td>
                          <td>Skills for Employment Investment Program | Under NIET</td>
                          <td>2018</td>
                          <td>3 months</td>
                      </tr>
                  </tbody>
              </table>

          </div>
      </div>
      <hr>
      <div class="row">
          <div class="col-md-12">
             <h3>KEY SKILLS AND COMPETENCIES</h3>
              <ul>
                  <li>Excellent Interpersonal Communication Skill Bengali & English </li>
                  <li>Very Quick learner </li>
                  <li>Hard worker </li>
                  <li>Ability to Adapt</li>
                  <li>Passionate to Programming </li>
                  <li>Able to work under pressure, meet deadlines and multitask</li>
                  <li>Supreme affection for programming and thirsty for knowledge</li>
                  <li>Excellent graphical skills, creative flair and good colour sense</li>

              </ul>
          </div>
      </div>
      <hr>
  </div>


 @endsection