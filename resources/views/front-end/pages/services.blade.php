@extends('front-end.layouts.masterall')
@section('title','About Me')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>SERVICES WHICH I PROVIDE</h1>

                <p style="color: black; text-align: center">
                    Welcome Services list page. I can provide all those services from my end.
                </p>
                <h3>PROFESSIONAL SKILLS</h3>
                <div class="col-md-3">
                    <h4>Web Development</h4>
                    <ul>
                        <li>Dynamic Website building</li>
                        <li>Web Based Applications</li>
                        <li>Management System (School/College/University)</li>
                        <li>Account inventory System</li>
                        <li>Company Website</li>
                        <li>Content Management System</li>
                        <li>Shopping Cart / Online Ordering System / Online Payments</li>
                        <li>Event Booking System</li>
                        <li>Event Calendar</li>
                        <li>Document Management System</li>
                        <li>Membership System</li>
                        <li>Blog / News Management</li>
                        <li>Business Directory / Listings</li>
                        <li>Booking / Reservations / Availability System</li>
                        <li>Social Media Integration — Facebook, Twitter, Youtube, Google+, etc.</li>
                        <li>Email Newsletter</li>
                        <li>Mobile-Optimised Website</li>
                        <li>Password-Protected Pages / Client Area</li>
                        <li>Web Application / Business Software</li>
                        <li>API Integration</li>
                        <li>And Many More…</li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Web Design</h4>
                    <ul>
                        <li>Web Design</li>
                        <li>WordPress Customization</li>
                        <li>HTML 5</li>
                        <li>CSS 3</li>
                        <li>JavaScript</li>
                        <li>PSD to HTML</li>
                        <li>And Many More…</li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Graphics Design</h4>
                    <ul>
                        <li>Logo Design</li>
                        <li>Corporate Identity Design</li>
                        <li>Flyer Design</li>
                        <li>Leaflet Design</li>
                        <li>Brochure Design</li>
                        <li>Banner design</li>
                        <li>Billboard design</li>
                        <li>And Many More…</li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Photo Editing</h4>
                    <ul>
                        <li>Photo Retouching</li>
                        <li>Photo Manipulation</li>
                        <li>Fashion Portrait,</li>
                        <li>weddings skin retouch</li>
                        <li>Colour Correction</li>
                        <li>Light Correction</li>
                        <li>Makeup beauty concept</li>
                        <li>Background Remove</li>
                        <li>Watermark Remove</li>
                        <li>JPEG To Vector Converting</li>
                        <li>GIF Animation</li>
                        <li>And Many More…</li>
                    </ul>
                </div>


            </div>
        </div>
        <hr>

    </div>


@endsection