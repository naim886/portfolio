@extends('front-end.layouts.master')
@section('title','Home')


@section('content')

    <div class="welcome">
        <div class="container">
            @include('front-end.includes.welcome')
        </div>
    </div>
    <!-- //welcome -->
    <!--client-->
    <div class="client">
        <div class="container">
            @include('front-end.includes.client')
        </div>
    </div>
    <!--//client-->
    <!-- what -->
    <div class="what-w3ls">
        <div class="container">
            @include('front-end.includes.work')
        </div>
    </div>

@endsection