@extends('front-end.layouts.masterall')
@section('title','About Me')
@section('content')
    <div class="container">
        <div class="container">
            <h2 class="w3ls_head">GET IN TOUCH</h2>
            <p style="color: black; text-align: center">If you have any quarry please don’t hesitate to contact with me.</p>
            <hr>
            <table style="margin: 0px auto">
                <tr >
                    <td style="width: 35%">E-mail :</td>
                    <td>naim886@gmail.com</td>
                </tr>
                <tr>
                    <td>Phone :</td>
                    <td>+8801719123886</td>
                </tr>
                <tr>
                    <td>Skype :</td>
                    <td>naim.374</td>
                </tr>
                <tr>
                    <td>Hangout :</td>
                    <td>naim886</td>
                </tr>
                <tr>
                    <td>Facebook :</td>
                    <td>naimcse</td>
                </tr>
                <tr>
                    <td>Twitter :</td>
                    <td>naim886</td>
                </tr>
            </table>
          <h3 style="text-align: center">You also can leave a messege here</h3>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @if(session()->has('status'))
                    <div class="alert alert-success text-center">
                        {{session('status')}}
                    </div>
                    @endif
                    {{Form::open(['url'=>'/contact','method'=>'post'])}}
                    {{Form::text('name', null, ['placeholder'=>'Enter Your Name', 'class'=>'formgoup', 'required'=>'""'])}}
                    {{Form::text('email', null, ['placeholder'=>'Enter Your Email', 'class'=>'formgoup', 'required'=>'""'])}}
                    {{Form::text('phone', null, ['placeholder'=>'Enter Phone number (Optional)', 'class'=>'formgoup'])}}
                    {{Form::text('subject', null, ['placeholder'=>'Enter Subject', 'class'=>'formgoup','required'=>'""'])}}
                    {{Form::textarea('message', null, ['placeholder'=>'Enter Your message', 'class'=>'formgoup'])}}
                    {{Form::submit('Send',['type'=>'button','class'=>'btn btn-primary btn-block', 'class'=>'formgoup'])}}
                    {{Form::close()}}
                </div>
            </div>




        </div>
        <hr>

    </div>


@endsection