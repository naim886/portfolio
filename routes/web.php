<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEndController@index')->name('front.home');
Route::get('/about','FrontEndController@about')->name('front.about');
Route::get('/services','FrontEndController@services')->name('front.services');
Route::get('/contact','FrontEndController@contact')->name('front.contact');
Route::get('/portfolio','FrontEndController@portfolio')->name('front.portfolio');
Route::post('/contact','FrontEndController@store')->name('front.store');
Route::post('/newsletter','FrontEndController@newsletter')->name('front.newsletter');